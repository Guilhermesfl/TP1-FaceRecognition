#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

void detectAndDisplay(Mat frame, string matricula, int *i );
void set_camera_print ();
void CallBackFunc(int event, int x, int y, int flags, void* userdata);
