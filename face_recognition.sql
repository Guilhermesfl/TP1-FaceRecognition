use face_recognition_db;

DROP TABLE IF EXISTS ADMINISTRADOR;
DROP TABLE IF EXISTS ALUNO;
DROP TABLE IF EXISTS PROFESSOR;
DROP TABLE IF EXISTS FUNCIONARIO;
DROP TABLE IF EXISTS RESERVA;
DROP TABLE IF EXISTS DISCIPLINA;
DROP TABLE IF EXISTS CADASTRO_DISCIPLINA;


CREATE TABLE ADMINISTRADOR (
	senha INT PRIMARY KEY AUTO_INCREMENT
);

CREATE TABLE ALUNO (
    matricula int PRIMARY KEY,
	senha VARCHAR(255) NOT NULL,
	nome VARCHAR (20) NOT NULL,
	sobrenome VARCHAR (20) NOT NULL
);

CREATE TABLE PROFESSOR (
    matricula int PRIMARY KEY,
	senha VARCHAR(255) NOT NULL,
	nome VARCHAR (20) NOT NULL,
	sobrenome VARCHAR (20) NOT NULL
);

CREATE TABLE FUNCIONARIO (
	cpf int PRIMARY KEY,
	senha VARCHAR(255) NOT NULL,
	nome VARCHAR (20) NOT NULL,
	sobrenome VARCHAR (20) NOT NULL
);

CREATE TABLE DISCIPLINA (
	matricula_professor int REFERENCES PROFESSOR(matricula),
    nome VARCHAR(30),
    turma CHAR NOT NULL,
    PRIMARY KEY (nome, turma)
);
		
CREATE TABLE CADASTRO_DISCIPLINA (
	nome VARCHAR(30) REFERENCES DISCIPLINA(id),
    matricula_aluno int REFERENCES ALUNO(matricula),
	turma CHAR NOT NULL,
	PRIMARY KEY (nome, matricula_aluno,turma)
);

CREATE TABLE RESERVA(
	codigo INT PRIMARY KEY auto_increment,
	descricao VARCHAR(255) NOT NULL,
	nome_disciplina VARCHAR(30) REFERENCES DISCIPLINA(nome),
	turma VARCHAR(1),
	num_sala INT,
    horario VARCHAR(20),
	-- Autores da Reserva
    matricula_aluno int REFERENCES ALUNO(matricula),
	matricula_professor int REFERENCES PROFESSOR(matricula),
	matricula_funcionario int REFERENCES FUNCIONARIO(cpf),
	dia VARCHAR(20),
    dt_init VARCHAR(20),
	dt_fim VARCHAR(20)
);

INSERT INTO ADMINISTRADOR(senha) VALUES ('1');

INSERT INTO DISCIPLINA(matricula_professor,nome,turma) VALUES('1','APC','A');
INSERT INTO DISCIPLINA(matricula_professor,nome,turma) VALUES('1','APC','B');
INSERT INTO DISCIPLINA(matricula_professor,nome,turma) VALUES('1','SD','A');
INSERT INTO DISCIPLINA(matricula_professor,nome,turma) VALUES('2','SD','B');
INSERT INTO DISCIPLINA(matricula_professor,nome,turma) VALUES('2','TP1','A');
INSERT INTO DISCIPLINA(matricula_professor,nome,turma) VALUES('2','CRB','A');
INSERT INTO DISCIPLINA(matricula_professor,nome,turma) VALUES('2','ID','A');

INSERT INTO ALUNO (matricula,senha,nome,sobrenome) VALUES('1', '1234','Didico','Imperador');
INSERT INTO ALUNO (matricula,senha,nome,sobrenome) VALUES('2','12345','Gabriel','Carvalho');

INSERT INTO PROFESSOR (matricula,senha,nome,sobrenome) VALUES('1', '123456', 'Guilherme', 'Lopes');
INSERT INTO PROFESSOR (matricula,senha,nome,sobrenome) VALUES('2', '123456', 'Ronaldinho', 'Gaucho');

INSERT INTO RESERVA (matricula_aluno, descricao, horario,dt_init,dt_fim) VALUES('150115471','Vou jogar coelho sabido', '16:20',null,null);
INSERT INTO RESERVA (matricula_professor, descricao, horario,dt_init,dt_fim) VALUES('150115476','Vou abrir os computadores para ver como eles sao feitos', '16:20','14/04','21/05');

SELECT * FROM ALUNO;
#SELECT matricula_aluno, matricula_professor, matricula_funcionario FROM RESERVA WHERE matricula_aluno IS NOT NULL AND matricula_funcionario IS NOT NULL OR matricula_professor;
#SELECT * FROM PROFESSOR;
#SELECT * FROM ADMINISTRADOR;
#SELECT * FROM FUNCIONARIO;

#SELECT senha FROM ALUNO where matricula=10;
