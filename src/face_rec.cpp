/*! @file face_rec.cpp
*   @authors Guilherme Lopes and Alex Souza
*   @brief face_rec.cpp descreve as funções de reconhecimento facial.
*/
#include "opencv2/core/core.hpp"
#include "opencv2/contrib/contrib.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "faces.hpp"
#include <ctime>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace cv;
using namespace std;

/** 
\brief Função responsável por ler o arquivo CSV que contém os caminhos para as imagens armazenadas.
\param filename representa o nome do arquivo
\param images é o vetor com todas as imagens a serem analisadas
\param labels é o vetor com todas as labels de cada imagem
\param separator é um char utilizado como separador entre o path de cada imagem e sua label
*/
void read_csv(const string& filename, vector<Mat>& images, vector<int>& labels, char separator = ';') {
    ifstream file(filename.c_str(), ifstream::in);
    if (!file) {
        string error_message = "No valid input file was given, please check the given filename.";
        CV_Error(CV_StsBadArg, error_message);
    }
    string line, path, classlabel;
    while (getline(file, line)) {
        stringstream liness(line);
        getline(liness, path, separator);
        getline(liness, classlabel);
        if(!path.empty() && !classlabel.empty()) {
            images.push_back(imread(path, 0));
            labels.push_back(atoi(classlabel.c_str()));
        }
    }
}

/** \brief Função responsável realizar o reconhecimento facial.*/
bool face_recognition(int matricula, string path) {

        /**
         \details A função realiza o reconhecimento facial utilando o metodo LBPH. O algoritmo tenta o reconhecimento facial por algum tempo
         \ e caso o reconhecimento falhe, retorna para o programa princial.
         \param matricula -> referência para identificação pessoal além da face
         \param path -> caminho para o diretório onde a imagem será salva
         \return Uma confirmação caso o reconhecimento facial tenha sucesso.
        */
    string fn_haar = "../haarcascade_frontalface_alt.xml";
    string fn_csv = path;
    int prediction = -99;
    bool flag = false;
    // These vectors hold the images and corresponding labels:
    vector<Mat> images;
    vector<int> labels;
    // Read in the data (fails if no valid input filename is given, but you'll get an error message):
    try {
        read_csv(fn_csv, images, labels);
    } catch (cv::Exception& e) {
        cerr << "Error opening file \"" << fn_csv << "\". Reason: " << e.msg << endl;
        exit(1);
    }
    // Get the height from the first image. We'll need this
    // later in code to reshape the images to their original
    // size AND we need to reshape incoming faces to this size:
    int im_width = images[0].cols;
    int im_height = images[0].rows;
    // Create a FaceRecognizer and train it on the given images:
    Ptr<FaceRecognizer> model = createLBPHFaceRecognizer();
    model->train(images, labels);

    CascadeClassifier haar_cascade;
    haar_cascade.load(fn_haar);

    // Get a handle to the Video device:
    VideoCapture capture(0);
    // Check if we can use this device at all:
    if(!capture.isOpened()) {
        cerr << "Capture cannot be opened" << endl;
        return false;
    }
    // Holds the current frame from the Video device:
    clock_t begin = clock();
    Mat frame;
    for(;;) {
        capture >> frame;
        // Clone the current frame:
        Mat original = frame.clone();
        // Convert the current frame to grayscale:
        Mat gray;
        cvtColor(original, gray, CV_BGR2GRAY);
        // Find the faces in the frame:
        vector< Rect_<int> > faces;
        haar_cascade.detectMultiScale(gray,faces);
        for(int i = 0; i < faces.size(); i++) {
            // Process face by face:
            Rect face_i = faces[i];
            // Crop the face from the image. So simple with OpenCV C++:
            Mat face = gray(face_i);
            // Now perform the prediction, see how easy that is:
            prediction = model->predict(face);
        }
        // Show the result:
        //imshow("FaceRecognizer", original);
        char key = (char) waitKey(10);
        if(prediction == matricula) flag = true;
        if(flag == true) break;
        clock_t end = clock();
        double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
        if(elapsed_secs > 20) break;
    }
    return flag;
}
