/*! @file faces.hpp
*   @authors Guilherme Lopes and Alex Souza
*   @brief faces.hpp descreve os headers das funções da parte de reconhecimento facial.
*/
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace cv;
using namespace std;

void detectAndDisplay(Mat frame, string matricula, int *i,string diretorio);
void set_camera_print(string matricula, string diretorio);
bool face_recognition(int matricula, string path);
