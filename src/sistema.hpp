/*! @file sistema.hpp
*   @author Guilherme Lopes and Alex Souza
*   @brief Esse arquivo descreve os headears das funções responsáveis pelo cadastro do usuário no sistema.
*/

//C++ REQUIRED LIBRARIES
#include <iostream>
#include <ostream>
#include <string>
//OPENCV REQUIRED LIBRARIES 
#include <opencv2/opencv.hpp>
#include <opencv/highgui.h>
//BOOST FILESYSTEM LIBRARIES
#include <boost/filesystem.hpp>
//MYSQL REQUIRED LIBRARIES 
#include "mysql_connection.h"
#include <mysql_driver.h>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>


using namespace std;
using namespace sql;

void usage();
void cadastraUsuario(int opcao,PreparedStatement* pstmt, Driver* driver, Connection* con, Statement* stmt, ResultSet* res);
void cadastraReserva(PreparedStatement* pstmt, Driver* driver, Connection* con, Statement* stmt, ResultSet* res);
void mostraReserva(int cadastro, PreparedStatement* pstmt, Driver* driver, Connection* con, Statement* stmt, ResultSet* res);
void dadosReserva(int opcao,int usuario, PreparedStatement* pstmt, Driver* driver, Connection* con, Statement* stmt, ResultSet* res);
